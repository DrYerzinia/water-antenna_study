## Dipole In Water Antenna Study

This is an attempt to understand how a dipole will behave when submerged in a fresh water enviroment (e.g. swimming pool) for short range underwater communications.  Expect the resonance of the dipole should be proportional to $`\sqrt{\epsilon_r}`$ as that is proportional to the wavelength in medium.  OpenEMS is being used to simulate the dipole.

Below are the results for the first simulation.  We set the input impedance to 75 Ohm and simulate a dipole with 1/2 wavelength arms using a voltage excitation.  The S11 results show resonance at ~23.6 MHz.  So we already have a deviation from our expected resonance at 27MHz.

| S11 | $`Z_{in}`$ |
|:-:|:-:|
| ![Air Dipole 27MHz wavelength S11](images/Dipole_27MHz_Air_S11.png "Air Dipole 27MHz wavelength S11") | ![Air Dipole 27MHz wavelength Zin](images/Dipole_27MHz_Air_Zin.png "Air Dipole 27MHz wavelength Zin") |  

Simulating this same size antenna but submered in a lossless dieletric material with an $`\epsilon_r = 80.2`$ we get the below results.  This shows a resonance at ~4MHz, when the expected resonance would be $`\frac{23.6 MHz}{\sqrt{\epsilon_r}} \approx 2.63 MHz`$

| S11 | $`Z_{in}`$ |
|:-:|:-:|
| ![Air Dipole 27MHz wavelength S11](images/Dipole_27MHz_Water_Lossless_Big_S11.png "Air Dipole 27MHz wavelength S11") | ![Air Dipole 27MHz wavelength Zin](images/Dipole_27MHz_Water_Lossless_Big_Zin.png "Air Dipole 27MHz wavelength Zin") |  

Now simulating the antenna at a size proportional to the wavelength in water we get 39MHz, a higher resonance frequency that expected.

| S11 | $`Z_{in}`$ |
|:-:|:-:|
| ![Air Dipole 27MHz wavelength S11](images/Dipole_27MHz_Water_Lossless_S11.png "Air Dipole 27MHz wavelength S11") | ![Air Dipole 27MHz wavelength Zin](images/Dipole_27MHz_Water_Lossless_Zin.png "Air Dipole 27MHz wavelength Zin") |  

In both cases the ratio of the expected to actual is ~1.5, is there a $`\sqrt{2}`$ missing?
