# -*- coding: utf-8 -*-

### Import Libraries
import os, tempfile
from pylab import *
import numpy as np
from scipy.constants import c
import argparse

from CSXCAD  import ContinuousStructure
from openEMS import openEMS
from openEMS.physical_constants import *

parser = argparse.ArgumentParser()
parser.add_argument('--nf2ff', action='store_true', help="Near2Far field conversion to get pattern.")
parser.add_argument('--post', action='store_true', help='Post-processing')
parser.add_argument('--simulate', action='store_true', help='Run simulation')
parser.add_argument('--show', action='store_true', help='Show 3D model / grid')
args = parser.parse_args()

nf2ff_enabled = False
show_enabled = False
simulate_enabled = False
post_process_enabled = False

if args.nf2ff:
    nf2ff_enabled = True
if args.show:
    show_enabled = True
if args.simulate:
    simulate_enabled = True
if args.post:
    post_process_enabled = True

### General parameter setup
Sim_Path = os.path.join(tempfile.gettempdir(), 'dipole_27MHz_air')

post_proc_only = False

# setup FDTD parameter & excitation function
f0 = 21e6
fc = 20e6

### FDTD setup
## * Limit the simulation to 30k timesteps
## * Define a reduced end criteria of -40dB
FDTD = openEMS(NrTS=30000, EndCriteria=1e-4)
FDTD.SetGaussExcite( f0, fc )
FDTD.SetBoundaryCond( ['PML_8', 'PML_8', 'PML_8', 'PML_8', 'PML_8', 'PML_8'] )

mil_to_m = 2.54e-5
mm_to_m = 1e-3

feed_R = 75

CSX = ContinuousStructure()
FDTD.SetCSX(CSX)
mesh = CSX.GetGrid()
mesh.SetDeltaUnit(1)

wavelength = c / 27e6

minimum_dimension =  wavelength / 100

side_length = wavelength / 2
wire_width = minimum_dimension
gap = minimum_dimension

simulation_domain = [
    [-wavelength, wavelength],
    [-wavelength, wavelength],
    [-wavelength, wavelength]
]

density = 10

for i in np.logspace(np.log10(wire_width * 3 / 2), np.log10(np.abs(simulation_domain[1][1])), density):
    mesh.AddLine('y', i)
for i in -1 * np.logspace(np.log10(np.abs(simulation_domain[1][0])), np.log10(wire_width * 3 / 2), density):
    mesh.AddLine('y', i)

for i in np.logspace(np.log10(np.abs(simulation_domain[2][1])), np.log10(wire_width * 3 / 2), density):
    mesh.AddLine('z', i)
for i in -1 * np.logspace(np.log10(np.abs(simulation_domain[2][0])), np.log10(wire_width * 3 / 2), density):
    mesh.AddLine('z', i)

for i in np.logspace(np.log10(np.abs(simulation_domain[0][1])), np.log10(side_length / 2 + side_length / 10), density):
    mesh.AddLine('x', i)
for i in -1 * np.logspace(np.log10(np.abs(simulation_domain[0][0])), np.log10(side_length / 2  + side_length / 10), density):
    mesh.AddLine('x', i)

def add_dipole(name, side_length, wire_width, gap):

    for i in np.linspace(- side_length / 2, - gap / 2 - side_length / 20, 10):
        mesh.AddLine('x', i)
    for i in np.linspace(gap / 2 + side_length / 20, side_length / 2, 10):
        mesh.AddLine('x', i)

    dipole_1 = CSX.AddMaterial(name + '_1', kappa=56e6)
    start = np.array([- side_length / 2, - wire_width / 2, - wire_width / 2])
    stop  = np.array([- gap / 2        ,   wire_width / 2,   wire_width / 2])
    dipole_1.AddBox(priority=10, start=start, stop=stop)

    dipole_2 = CSX.AddMaterial(name + '_2', kappa=56e6)
    start = np.array([side_length / 2, - wire_width / 2, - wire_width / 2])
    stop  = np.array([gap / 2        ,   wire_width / 2,   wire_width / 2])
    dipole_2.AddBox(priority=10, start=start, stop=stop)

# Add dipole
add_dipole('dipole', side_length, wire_width, gap)

# Add Input port
start = np.array([- gap / 2, - wire_width / 2, - wire_width / 2])
stop  = np.array([  gap / 2,   wire_width / 2,   wire_width / 2])
port = FDTD.AddLumpedPort(1, feed_R, start, stop, 'x', 1.0, priority=5, edges2grid='xyz')

if nf2ff_enabled:
    # Add the nf2ff recording box
    nf2ff = FDTD.CreateNF2FFBox()

if show_enabled:
    # Show the mesh
    CSX_file = os.path.join(Sim_Path, 'loop.xml')
    if not os.path.exists(Sim_Path):
        os.mkdir(Sim_Path)
    CSX.Write2XML(CSX_file)
    os.system(r'AppCSXCAD "{}"'.format(CSX_file))

if simulate_enabled:
    ### Run the simulation
    FDTD.Run(Sim_Path, verbose=3, cleanup=True)

if post_process_enabled:
    ### Post-processing and plotting
    f = np.linspace(f0-fc,f0+fc,401)
    port.CalcPort(Sim_Path, f)
    s11 = port.uf_ref/port.uf_inc
    s11_dB = 20.0*np.log10(np.abs(s11))
    figure()
    plot(f/1e6, s11_dB, 'k-', linewidth=2, label='$S_{11}$')
    grid()
    legend()
    ylabel('S-Parameter (dB)')
    xlabel('Frequency (MHz)')

    if nf2ff_enabled:
        idx = np.where((s11_dB<-10) & (s11_dB==np.min(s11_dB)))[0]
        if not len(idx)==1:
            print('No resonance frequency found for far-field calulation')
        else:
            f_res = f[idx[0]]
            theta = np.arange(-180.0, 180.0, 2.0)
            phi   = [0., 90.]
            nf2ff_res = nf2ff.CalcNF2FF(Sim_Path, f_res, theta, phi, center=[0,0,0])

            figure()
            E_norm = 20.0*np.log10(nf2ff_res.E_norm[0]/np.max(nf2ff_res.E_norm[0])) + nf2ff_res.Dmax[0]
            plot(theta, np.squeeze(E_norm[:,0]), 'k-', linewidth=2, label='xz-plane')
            plot(theta, np.squeeze(E_norm[:,1]), 'r--', linewidth=2, label='yz-plane')
            grid()
            ylabel('Directivity (dBi)')
            xlabel('Theta (deg)')
            title('Frequency: {} MHz'.format(f_res/1e6))
            legend()

    Zin = port.uf_tot/port.if_tot
    figure()
    plot(f/1e6, np.real(Zin), 'k-', linewidth=2, label='$\Re\{Z_{in}\}$')
    plot(f/1e6, np.imag(Zin), 'r--', linewidth=2, label='$\Im\{Z_{in}\}$')
    grid()
    legend()
    ylabel('Zin (Ohm)')
    xlabel('Frequency (MHz)')

    show()
